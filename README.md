#Gitの練習用じゃんけんアプリです#

各自でGitのインストールと初期設定を済ませておいてください  
git diffに色つけしたいまではやってください  
http://qiita.com/wnoguchi/items/f7358a227dfe2640cce3  

この勉強回で扱うコマンド  
git init  
git add  
git diff  
git commit  
git status  
git branch  元データのコピーをするコマンド
git checkout  
git log  
git push  
git pull  

###リモートリポジトリ(bitbucket)の事前準備###
アカウントを作成しておいてください
[bitbucket]: https://bitbucket.org/        "bitbucket" 


sshの設定をしてくだい!!  
http://morizyun.github.io/blog/ssh-key-bitbucket-github/  
記事が古いので(3)の左側のメニューの「SSH keys」 => 「Add key」を選択。  
はbitbucket settingsにSSH キー  
(3)までやって接続テストを済ませてください

--接続テスト--  
$ ssh -T git@bitbucket.org  

※、正しくSSHが設定されていれば、以下のように表示されます  
conq: logged in as ユーザ名.  

You can use git or hg to connect to bitbucket. Shell access is disabled.  

もしくは  
Warning: Permanently added the RSA host key for IP address '***.***.***.*' to the list of known hosts.  
logged in as ユーザ名.  

You can use git or hg to connect to Bitbucket. Shell access is disabled.

#Gitを使った個人開発の流れ#
gitで管理したいディレクトリに移動  
↓  
git initでgit管理開始  
※.gitignoreを作成しlogファイルなどはgit管理しないでおく  
http://qiita.com/y_minowa/items/e3d8513dc31c1b378e0d  
gitignore android or ios など調べるとわかる  
↓  
git add→git commitを行う  
↓  
git statusでnothing to commit, working directory cleanで変更点がない事を確認  
↓  
ブランチを切る git branch  
↓  
git add→git commit→の繰り返し  
↓  
まとまった実装(1つの機能など)が完了したらまたブランチを切ってadd、commitを繰り返す  

#リモートリポジトリ(bitbucket)を使った個人開発#
bitbucketでリポジトリを作成する  
↓  
ローカルのブランチをリモートにプッシュ  
git push origin ブランチ名  
↓  
bitbucketのサイト上でプルリクエストを作成  
masterブランチにプルリクを送る  
↓  
コードレビュー  
↓  
merge  
↓  
mergeできたら  
git pull origin master  
でローカルを最新verに変更  
↓  
git branch ブランチ名でブランチを切る  
↓  
git add git commit 繰り返し  
↓  
まとまった実装ができたら  
push プルリク作成 pullを行う  
↓  
繰り返す  

参考資料  
サルでもわかるGit入門  
http://www.backlog.jp/git-guide/  


